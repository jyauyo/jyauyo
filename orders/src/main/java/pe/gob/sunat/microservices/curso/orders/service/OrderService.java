package pe.gob.sunat.microservices.curso.orders.service;

import pe.gob.sunat.microservices.curso.customers.client.Address;
import pe.gob.sunat.microservices.curso.orders.dao.OrderDaoImpl;
import pe.gob.sunat.microservices.curso.orders.model.Order;

import java.util.Date;
import java.util.List;

public class OrderService {

	private final CustomerService customerService;
	private final OrderDaoImpl dao;

	public OrderService(CustomerService customerService, OrderDaoImpl dao) {
		this.customerService = customerService;
		this.dao = dao;
	}

	/**
	 * @author jyauyo
	 * 
	 * @param order Order: Pedido a guardar
	 * @param credentials String: Credenciales del Usuario en sesion
	 * 
	 * <br><br>
	 * Clase que crea en pedido.<br> Se valida que el el cliente y su direccion existan
	 * 
	 * */
	public Order create(Order order, String credentials) {
		
		validaCustomer(order.getCustomerId(), credentials);
		validaAddressCustomer(order.getCustomerId(), order.getDeliveryAddressId(),credentials);
		
		order.setCreatedAt(new Date());
		return dao.create(order);
	}

	public List<Order> ordersByCustomer(Long customerId, String credentials) {
		return dao.findByCustomer(customerId);
	}

	public void delete(Long customerId, String credentials) {
		dao.delete(customerId);
	}

	/**
	 * @author jyauyo
	 * 
	 * @param customerId Long: Id del cliente
	 * @param credentials String: Credenciales del Usuario en sesion
	 * Validar el usuario logueado
	 * 
	 * <br><br>
	 * Clase que valida si el cliente existe
	 * 
	 * */
	private void validaCustomer(Long customerId, String credentials) {

		Boolean validatedCustomer = customerService.existeCustomer(
				customerId, credentials);

		if (!validatedCustomer) {
			throw new InvalidCustomerException(
					"No existe el cliente. Se cancela la creacion del pedido.",
					customerId.toString());
		}
	}
	
	/**
	 * @author jyauyo
	 * 
	 * @param customerId Long: Id del cliente
	 * @param credentials String: Credenciales del Usuario en sesion
	 * Validar el usuario logueado
	 * 
	 * <br><br>
	 * Clase que valida si la direccion del cliente existe
	 * 
	 * */	
	private void validaAddressCustomer(Long customerId, Long addressId, String credentials){
		
		List<Address> address = customerService.addressByCustomer(customerId, credentials);
		
		boolean existeAddress = address.stream().anyMatch(a -> a.getId().equals(addressId));
			
		if(!existeAddress){
			throw new InvalidAddressCustomerException(
					"No existe Direccion del cliente. Se cancela la creacion del pedido.",
					customerId.toString());
		}
	}
}
