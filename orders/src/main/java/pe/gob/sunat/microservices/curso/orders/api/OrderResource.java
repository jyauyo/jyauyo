package pe.gob.sunat.microservices.curso.orders.api;

import pe.gob.sunat.microservices.curso.orders.model.Order;
import pe.gob.sunat.microservices.curso.orders.service.OrderService;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

import java.util.List;

@RolesAllowed("ADMIN")
@Path("/v1/orders")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {
	private final OrderService orderService;

	public OrderResource(OrderService orderService) {
		this.orderService = orderService;
	}

	@POST
	public Order create(@Valid Order customer, @Context HttpHeaders headers) {
		String credentials = headers.getHeaderString("Authorization");
		return orderService.create(customer, credentials);
	}

	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") Long id, @Context HttpHeaders headers) {
		String credentials = headers.getHeaderString("Authorization");
		orderService.delete(id, credentials);
	}

	@GET
	@Path("/_customer")
	public List<Order> ordersList(@QueryParam("id") Long idCustomer,
			@Context HttpHeaders headers) {
		String credentials = headers.getHeaderString("Authorization");
		return orderService.ordersByCustomer(idCustomer, credentials);
	}
}
