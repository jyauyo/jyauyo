# exportar la variable del host de Postgres
export DATABASE_HOST=localhost


# Levantamos el Docker Compose
./up.sh

# Validanmos con
docker-compose ps

#Asegurarnos que corren todos los contenedores (Up)

# Generamos los tags de la migración
java -jar ./build/libs/service.jar db tag ./src/main/resources/config.yaml 2018_08_27_19_42

# Aplicamos la migración a la base de datos
java -jar ./build/libs/service.jar db migrate ./src/main/resources/config.yaml


# ============== Comandos utilitarios =================

# Usuarios: usr: customer_user, pwd: customer; usr: user_user pwd: user; ,usr: order_user pwd: orders
psql -h localhost -U customer_user -W customers

# Listar volumenes de datos que Docker gestiona
docker volume ls

# Limpiar los volumenes (si deseo borrarlos, no se recuperan)
docker volume rm id_del_volumen

