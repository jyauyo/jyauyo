package pe.gob.sunat.microservices.curso.customers.service.command;

import java.util.List;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

import pe.gob.sunat.microservices.curso.orders.client.Order;
import pe.gob.sunat.microservices.curso.orders.client.OrderServiceClient;
import retrofit2.Response;

/**
 * @author jyauyo
 * 
 * Clase Command Generica para CustomerHystrix
 * 
 * */
public class OrderServiceRemoteInvokerCommand extends HystrixCommand<Boolean> {
	public static final int CONCURRENT_REQUESTS = 20;
	private final Long idCustomer;
	private final String credentials;

	private final OrderServiceClient orderServiceClient;

	public OrderServiceRemoteInvokerCommand(OrderServiceClient orderServiceClient, Long idCustomer,
			String credentials) {
                
		super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("GroupComandoLatencia2"))
				.andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
						.withExecutionIsolationSemaphoreMaxConcurrentRequests(CONCURRENT_REQUESTS)));
		this.orderServiceClient = orderServiceClient;
		this.credentials = credentials;
		this.idCustomer = idCustomer;
                System.out.println("################### invocar ms Order");
	}

	@Override
	protected Boolean run() throws Exception {
                System.out.println("################### run OrderServiceInvoker "+orderServiceClient);
                System.out.println("################### run idCustomer "+idCustomer+" - "+credentials);

		boolean flag = Boolean.FALSE;
		Response<List<Order>> rsp = orderServiceClient.get(idCustomer, credentials).execute();
		System.out.println("################## Response "+rsp);
		if(rsp!=null && !rsp.body().isEmpty()){
			flag = Boolean.TRUE;
		}		
		return flag;
	}

	@Override
	protected Boolean getFallback() {
                System.out.println("################# FallBack OrderService Invoke");
		return true;
	}
}
